﻿angular.module('SignalR', [])
    .constant('$', $)
    .factory('Hub', [
        '$', function($) {
            //This will allow same connection to be used for all Hubs
            //It also keeps connection as singleton.
            var globalConnection = $.hubConnection();
            return function(hubName, listeners, methods) {
                var hub = this;
                hub.connection = globalConnection;
                hub.proxy = hub.connection.createHubProxy(hubName);
                hub.on = function(event, fn) {
                    hub.proxy.on(event, fn);
                };
                hub.invoke = function(method, args) {
                    hub.proxy.invoke.apply(hub.proxy, arguments)
                };

                if (listeners) {
                    angular.forEach(listeners, function(fn, event) {
                        hub.on(event, fn);
                    });
                }
                if (methods) {
                    angular.forEach(methods, function(method) {
                        hub[method] = function() {
                            var args = $.makeArray(arguments);
                            args.unshift(method);
                            hub.invoke.apply(hub, args);
                        };
                    });
                }
                //Adding additional property of promise allows to access it in rest of the application.
                hub.promise = hub.connection.start();
                return hub;
            };
        }
    ]);