﻿using Elmah;
using Microsoft.AspNet.SignalR.Hubs;

namespace TestSignalR
{
    public class ElmahErrorModule : HubPipelineModule
    {
        private readonly ErrorSignal _errorSignal;

        public ElmahErrorModule(ErrorSignal errorSignal)
        {
            _errorSignal = errorSignal;
        }

        protected override void OnIncomingError(ExceptionContext exceptionContext, IHubIncomingInvokerContext invokerContext)
        {
            _errorSignal.Raise(exceptionContext.Error);
        }
    }
}