﻿using System.Web;
using Elmah;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using TestSignalR;

[assembly: OwinStartup(typeof(Startup))]

namespace TestSignalR
{
    public class Startup
    {
        private ElmahExceptionHandler _elmahExceptionHandler;

        public void Configuration(IAppBuilder app)
        {
            var errorSignal = ErrorSignal.Get(HttpContext.Current.ApplicationInstance);
            _elmahExceptionHandler = new ElmahExceptionHandler(HttpContext.Current.ApplicationInstance, errorSignal);
            GlobalHost.HubPipeline.AddModule(new ElmahErrorModule(errorSignal));

            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            app.MapSignalR();
        }
    }
}
