﻿using System;
using Elmah;
using Microsoft.AspNet.SignalR;

namespace TestSignalR
{
    public class ErrorHub : Hub
    {
        internal static void LogError(Error error)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<ErrorHub>();
            context.Clients.All.logError(error);
        }

        public void Send(string name, string message)
        {
            if(string.IsNullOrEmpty(name)) throw new ArgumentException("Name is required!", "name");
            if (string.IsNullOrEmpty(message)) throw new ArgumentException("Message is required!", "message");

            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(name, message);
        }
    }
}