﻿using System;
using System.Web;
using Elmah;

namespace TestSignalR
{
    public class ElmahExceptionHandler
    {
        public ElmahExceptionHandler(HttpApplication application, ErrorSignal errorSignal)
        {
            application.Error += OnError;
            errorSignal.Raised += OnErrorSignaled;
        }

        private static void OnErrorSignaled(object sender, ErrorSignalEventArgs args)
        {
            LogException(args.Exception, args.Context);
        }

        private static void LogException(Exception exception, HttpContext context)
        {
            ErrorHub.LogError(new Error(exception, context));
        }

        private static void OnError(object sender, EventArgs e)
        {
            var httpApplication = (HttpApplication)sender;
            LogException(httpApplication.Server.GetLastError(), httpApplication.Context);
        }

    }
}